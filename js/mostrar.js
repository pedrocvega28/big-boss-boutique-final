import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = 
{
    apiKey: "AIzaSyCEujK7dp4PYwZtuc-6N2b9h-5ZzRIovHI",
    authDomain: "formlogin-37aa8.firebaseapp.com",
    projectId: "formlogin-37aa8",
    storageBucket: "formlogin-37aa8.appspot.com",
    messagingSenderId: "106374432738",
    appId: "1:106374432738:web:61523265ebd669b4139bbb"

  };
  
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    mostrarProductos();
  });
 
  function mostrarProductos() {
    const dbRef = refS(db, 'productos');
    const contenedorProductos = document.getElementById('contenedorProductos');
 
    contenedorProductos.innerHTML = '';
 
    onValue(dbRef, (snapshot) => {
      contenedorProductos.innerHTML = '';
 
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
 
        // Create column div for the product
        const divColumn = document.createElement('div');
        divColumn.classList.add('column');
 
        // Create product div
        const divProducto = document.createElement('div');
        divProducto.classList.add('producto');
 
        // Create heading element for the product name
        const h3Producto = document.createElement('h3');
        h3Producto.textContent = data.nombre;
        divProducto.appendChild(h3Producto);
 
        // Create image element
        const imagen = document.createElement('img');
        imagen.src = data.url;
        imagen.alt = data.nombre;
        divProducto.appendChild(imagen);
 
        // Create paragraph element for the product price
        const pPrecio = document.createElement('p');
        pPrecio.textContent = `Precio: $${data.Precio.toString()}`;
        divProducto.appendChild(pPrecio);
 
        // Append the product div to the column div
        divColumn.appendChild(divProducto);
 
        // Append the column div to the container
        contenedorProductos.appendChild(divColumn);
      });
    });
  }