import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {

    apiKey: "AIzaSyCEujK7dp4PYwZtuc-6N2b9h-5ZzRIovHI",
    authDomain: "formlogin-37aa8.firebaseapp.com",
    projectId: "formlogin-37aa8",
    storageBucket: "formlogin-37aa8.appspot.com",
    messagingSenderId: "106374432738",
    appId: "1:106374432738:web:61523265ebd669b4139bbb"
  };
  
 
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    ListarProductos();
    mostrarProductos();
  });
  
 
var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
 
var idProducto = "";
var nombreProducto = "";
var precio = "";
var url = "";
var Stock = "";
 
function leerInputs() {
  idProducto = document.getElementById('txtNumProducto').value;
  nombreProducto = document.getElementById('txtNombre').value;
  precio = document.getElementById('txtPrecio').value;
  url = document.getElementById('url').value;
  Stock = document.getElementById('Stock').value;
}

 
function insertarDatos() {
  leerInputs();
  set(refS(db, 'productos/' + idProducto), {
    nombre: nombreProducto,
    Precio: precio,
    url: url,
    Stock: Stock
  }).then(() => {
    alert("Se insertó con éxito");
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
  limpiarInputs();
  ListarProductos();
}
 
function buscarDatos() {
  idProducto = document.getElementById('txtNumProducto').value;
  const dbRef = refS(db, 'productos/' + idProducto);
  get(dbRef).then((snapshot) => {
    if (snapshot.exists()) {
      const data = snapshot.val();
      nombreProducto = data.nombre;
      precio = data.Precio;
      url = data.url;
      Stock = data.Stock;
      escribirInputs();
    } else {
      alert("el producto " + idProducto + " no existe");
    }
  }).catch((error) => {
    alert("ocurrio un error: " + error);
  });
}
 
function escribirInputs() {
  document.getElementById('txtNombre').value = nombreProducto;
  document.getElementById('txtPrecio').value = precio;
  document.getElementById('url').value = url;
  document.getElementById('Stock').value = Stock;
}
 
function ListarProductos() {
  const dbRef = refS(db, 'productos');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
 
  tbody.innerHTML = '';
 
  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const key = childSnapshot.key;
      const data = childSnapshot.val();
 
      var fila = document.createElement('tr');
 
      var celdaNumProducto = document.createElement('td');
      celdaNumTorta.textContent = key;
      fila.appendChild(celdaNumProducto);
 
      var celdaNombreProducto = document.createElement('td');
      celdaNombreTorta.textContent = data.nombre;
      fila.appendChild(celdaNombreProducto);
 
      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = data.Precio;
      fila.appendChild(celdaPrecio);
 
      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}
 
function actualizarDatos() {
  leerInputs();
  if (idProducto === "" || nombreProducto=== "" || precio === "") {
    alert("Falta capturar información");
  } else {
    update(refS(db, 'productos/' + idProducto), {
      nombre: nombreProducto,
      Precio: precio,
      url: url,
      Stock: Stock
    }).then(() => {
      alert("Se actualizó con éxito");
      limpiarInputs();
    }).catch((error) => {
      alert("Ocurrió un error: " + error);
    });
  }
  ListarProductos();
}
 
function eliminarProducto() {
  idProducto = document.getElementById('txtNumProducto').value;
  remove(refS(db, 'productos/' + idProducto)).then(() => {
    alert("Producto eliminado con éxito");
    limpiarInputs();
    ListarProductos();
  }).catch((error) => {
    alert("Ocurrió un error al eliminar el producto: " + error);
  });
}
 
function limpiarInputs() {
  document.getElementById('txtNumProducto').value = '';
  document.getElementById('txtNombre').value = '';
  document.getElementById('txtPrecio').value = '';
  document.getElementById('url').value = '';
  document.getElementById('Stock ').value = '';

}


document.getElementById("btnVolver").addEventListener("click", () => {
  window.location.href = "html./index.htm";
});
  
 
btnBorrar.addEventListener('click', eliminarProducto);
btnAgregar.addEventListener('click', insertarDatos);
btnActualizar.addEventListener('click', actualizarDatos);
btnBuscar.addEventListener('click', buscarDatos);